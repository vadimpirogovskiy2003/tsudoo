#include "logic.h"
#include "functions.h"

int main_menu(void) {
  char *banner = "Main menu\n\n";
  char *menu = "[1] Create anketa\n[2] Set information\n[3] View my information \n"
               "[4] List all anketa\n[5] Exit\n\n> ";
  printf(banner);
  printf(menu);
  char choise[4] = {0};
  read_data(0, choise, 4);
  return atoi(choise);
}



void create_anketa(void) {
  char anketa[ANKETA_SIZE] = {0};
  /* TODO: После ввода анкеты выводить ее и спрашивать корректен ли текст,
   * т.к. ограничение 2048 символов и весь текст может не поместиться
   */
  printf("Enter text for your anketa:\n");
  read_until_data(0, anketa, "END", ANKETA_SIZE);
  save_anketa(anketa);
}

void save_anketa(char *anketa) {
  char anketa_path[512] = {0};
  getcwd(anketa_path, sizeof(anketa_path));
  strcat(anketa_path, "/users/");
  strcat(anketa_path, login);
  strcat(anketa_path, "/anketa");
  FILE *f = fopen(anketa_path, "w");
  if (f == NULL) {
    printf("Error! don't create file anketa\n");
    exit(-1);
  }
  fprintf(f, anketa);
  fclose(f);
}

void create_information(void) {
  char info[INFO_SIZE] = {0};
  /* TODO: После ввода информации выводить ее и спрашивать корректен ли текст,
   * т.к. ограничение 1024 символов и весь текст может не поместиться
   */
  printf("Enter text for your information:\n");
  read_until_data(0, info, "END", INFO_SIZE);
  save_information(info);
}

void save_information(char *info) {
  size_t info_size = strlen(info);
  char info_path[512] = {0};
  getcwd(info_path, sizeof(info_path));
  strcat(info_path, "/users/");
  strcat(info_path, login);
  strcat(info_path, "/info");
  FILE *f = fopen(info_path, "w");
  if (f == NULL) {
    printf("Error! don't create file info\n");
    exit(-1);
  }
  fprintf(f, info);
  fclose(f);
}

void view_information(void){

}



char *get_ankets_paths(int count) {
  char *result = malloc(5 * 1024);
  memset(result, 0, 5 * 1024);
  FILE *f = popen("ls -t ./users/*/anketa", "r");
  if (f == NULL) {
    printf("Error! failed to execute command!\n");
    exit(-1);
  }
  char buf[512] = {0};
  do {
    memset(buf, 0, sizeof(buf));
    fgets(buf, sizeof(buf), f);
    if (*buf != '\0' && *buf != EOF) {
      buf[strlen(buf) - 1] = ' ';
    } else {
      break;
    }
    strcat(result, buf);
  } while (*buf != EOF && *buf != '\0');
#ifdef DEBUG
  printf("[DEBUG] get_list_ankets ret: %s\n", result);
#endif
  pclose(f);
  size_t result_size = strlen(result);
  if (count <= 0) {
    return result;
  }
  size_t spaces = 0;
  char *last_space = NULL;
  for (size_t i = 0; i < result_size && spaces < count; i++) {
    if (result[i] == ' ') {
      spaces++;
      last_space = result + i;
    }
  }
  if (spaces == count) {
    *last_space = '\0';
  }
  return result;
}

void view_my_info(void){
  get_my_info(login);
}

char *get_my_info(char *username) {
  char base_path[512] = {0};
  getcwd(base_path, sizeof(base_path));
  strcat(base_path, "/users/");
  strcat(base_path, username);

  //char anketa_path[512] = {0};
  //strcpy(anketa_path, base_path);
  //strcat(anketa_path, "/anketa");

  char info_path[512] = {0};
  strcpy(info_path, base_path);
  strcat(info_path, "/info");
  //printf("%s", anketa_path);
  FILE *f = fopen(info_path, "r");
  if (f == NULL) {
    printf("Error! you don`t have info!\n");
    exit(-1);
  }
  char anketa[512];
  fgets(anketa, 512, f);
  printf("%s", anketa); 
}

void print_ankets(char *ankets_list) {
  char *ptr = NULL;
  ptr = strtok(ankets_list, " ");
  size_t i = 1;
  while (ptr != NULL) {
    char *anketa_title = get_anketa_title(ptr);
    strstr(ptr + 8, "/")[0] = '\0';
    char *username = ptr + 8;
    printf("%lu) [%s]: %s\n", i++, username, anketa_title);
#ifdef DEBUG
    printf("[DEBUG] login = %s\n", username);
#endif
    ptr = strtok(NULL, " ");
  }
}

char *get_anketa_title(char *path_to_anketa) {
  FILE *f = fopen(path_to_anketa, "r");
  if (f == NULL) {
    printf("Error! don't open %s file\n", path_to_anketa);
    exit(-1);
  }
  char *title = malloc(32);
  fgets(title, 32, f);
  char *new_line = strstr(title, "\n");
  if (new_line != NULL) {
    *new_line = '\0';
  }
  return title;
}