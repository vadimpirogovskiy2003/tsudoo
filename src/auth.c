#include "auth.h"

char is_login = 0;
char login[32] = {0};

int login_menu(void) {
  char *banner =
      "| |     / /__  / /________  ____ ___  ___  \n| | /| / / _ \\/ / ___/ __ "
      "\\/ __ `__ \\/ _ \\\n| |/ |/ /  __/ / /__/ /_/ / / / / / /  "
      "__/\n|__/|__/\\___/_/\\___/\\____/_/ /_/ /_/\\___/ \n";
  char *mainmenu = "[1]: Register\n[2]: Login\n[3]: Exit\n\n> ";
  printf(banner);
  printf(mainmenu);
  char choice[4] = {0};
  read_data(0, choice, 4);
  return atoi(choice);
}

void user_reg(void) {
  char username[32] = {0};
  char passwd[32] = {0};

  printf("[*] Enter your username: ");
  read_data(0, username, 32);
  printf("[*] Enter your passwd: ");
  read_data(0, passwd, 32);

  char userdir[512] = {0};
  getcwd(userdir, sizeof(userdir));
  strcat(userdir, "/users/");
  strcat(userdir, username);
  char cmd[512] = {0};
  strcat(cmd, "mkdir -p ");
  strcat(cmd, userdir);
  system(cmd);
  char file_passwd[512] = {0};
  strcat(file_passwd, userdir);
  strcat(file_passwd, "/passwd");
  FILE *f = fopen(file_passwd, "w");
  if (f == NULL) {
    printf("Error! Could not open file\n");
    exit(-1);
  }
  fprintf(f, "%s\n", passwd);
  fclose(f);
}

void user_log(void) {
  char username[32] = {0};
  char passwd[32] = {0};

  printf("[*] Enter your username: ");
  read_data(0, username, 32);
  printf("[*] Enter your passwd: ");
  read_data(0, passwd, 32);

  char file_passwd[512] = {0};
  getcwd(file_passwd, sizeof(file_passwd));
  strcat(file_passwd, "/users/");
  strcat(file_passwd, username);
  strcat(file_passwd, "/passwd");
  FILE *f = fopen(file_passwd, "r");
  if (f == NULL) {
    printf("Error! incorrect login\\passwd\n");
    exit(-1);
  }
  char check_passwd[32] = {0};
  fscanf(f, "%s", check_passwd);
  if (strcmp(check_passwd, passwd)) {
    printf("Error! incorrect login\\passwd\n");
    exit(-1);
  }
  is_login = 1;
  strcpy(login, username);
  fclose(f);
}
