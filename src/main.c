#include "auth.h"
#include "logic.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

void __attribute__((constructor)) init(void) {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

int main(int argc, char **argv, char **env) {
  while (!is_login) {
    int choice = login_menu();
    // Сюда можно простую закладку
    switch (choice) {
    /* Register */
    case 1:
      user_reg();
      break;
    /* Login */
    case 2:
      user_log();
      break;
      /* Exit */
    case 3:
      return EXIT_SUCCESS;
    }
  }
  printf("[!] Your successfully login\n");
  for (;;) {
    int choice = main_menu();
#ifdef DEBUG
    printf("[DEBUG] choise = %d\n", choice);
#endif
    char *ankets_list = NULL;
    switch (choice) {
    /* Create anketa */
    case 1:
      create_anketa();
#ifdef DEBUG
      printf("[DEBUG] post create_anketa()\n");
#endif
      break;
    /* Create information */
    case 2:
      create_information();
      /* List last ankets */
      break;
    case 3:
      view_my_info();
      break;
    /* List all ankets */
    case 4:
      ankets_list = get_ankets_paths(0);
      print_ankets(ankets_list);
      free(ankets_list);
      break;
    /* Exit */
    case 5:
      return EXIT_SUCCESS;
    default:
      printf("Incorrect command!\n");
      break;
    }
  }
  return EXIT_SUCCESS;
}
