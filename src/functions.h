#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>

extern int read_data(int fd, char *buf, int size);
extern int read_until_data(int fd, char *buf, char *sub, int size);
extern void print_chars(char *ptr, size_t size);

#endif
