#ifndef LOGIC_H
#define LOGIC_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define ANKETA_SIZE 2048
#define INFO_SIZE 1024

extern int main_menu(void);
extern void create_anketa(void);
extern void save_anketa(char *anketa);
extern void create_information(void);
extern void save_information(char *info);
extern char *get_ankets_paths(int count);
extern char *get_info(char *username);
extern void print_ankets(char *ankets_list);
extern char *get_anketa_title(char *path_to_anketa);
extern void view_my_info();
extern char *get_my_info(char *username);

extern char login[32];
extern char is_login;
#endif
