#include "functions.h"

static char press_ctrlc = 0;

int read_data(int fd, char *buf, int size) {
  int i;
  for (i = 0; i < size && read(fd, buf, 1) == 1 && *buf != '\n'; i++, buf++)
    ;

  *buf = 0;
  return i;
}

int read_until_data(int fd, char *data, char *sub, int size) {
  int i = 0;
  char *buf = data;
  while ((strstr(data, sub) == NULL) && i < size) {
    for (; i < size && read(fd, buf, 1) == 1 && *buf != '\n'; i++, buf++)
      ;
    buf++;
#ifdef DEBUG
    printf("[DEBUG] read_until_data buf = %s | strstr = %p\n", data,
           strstr(data, sub));
#endif
  }
  char *p = strstr(data, sub);
  if (p != NULL) {
    size_t end_len = strlen(p);
    memset(p, 0, end_len);
  }
  *buf = 0;
  return i;
}

void print_chars(char *ptr, size_t size) {
  for (size_t i = 0; i < size; i++) {
    putchar(ptr[i]);
  }
}
