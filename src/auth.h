#ifndef AUTH_H
#define AUTH_H

#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern int login_menu(void);
extern void user_reg(void);
extern void user_log(void);

extern char is_login;
extern char login[32];

#endif
