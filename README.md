# Tsudoo

## Usage

<!--TODO: Сделать скрипт который бы удалял папки автоматически -->
Перед игрой удалить `src/` `Makefile` 
- `docker-compose up -d` чтобы поднять
- `docker-compose down` чтобы опустить
- Флаги в скрытой пользовательской информации

## Build

- `make`

## How does it work

Сервис поднимается через docker контейнер (для удобства добавлен docker-compose)
*За docker отвечают файлы* `Dockerfile` *и* `docker-compose`

На докере поднимается xinetd который вешает бинарный файл из `build/` на порт.
*Файл* `xinetd`
