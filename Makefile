CC = gcc
CFLAGS = -O0 -fno-stack-protector -no-pie -fno-pie 
CFLAGS_DEBUG = -DDEBUG -g

.PHONY: build
build:
	$(CC) ./src/*.c $(CFLAGS) -o ./build/tsudoo

.PHONY: build_debug
build_debug:
	$(CC) ./src/*.c $(CFLAGS) $(CFLAGS_DEBUG) -o ./build/tsudoo
